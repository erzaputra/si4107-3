1. PENDAHULUAN

1.1 Latar Belakang
	PT.TOURDERA merupakan biro perjalanan wisata yang mempunyai tujuan untuk menyiapkan suatu perjalanan bagi orang-orang atau seorang yang merencanakan untuk mengadakannya (Nyoman, 1990). Biro perjalanan wisata saat ini memiliki peran penting dan mengalami perkembangan yang sangat pesat di Indonesia. Salah satu destinasi wisata yang sering dikunjungi yaitu provinsi Jawa Barat terkhususnya Bandung. Bandung memiliki banyak destinasi wisata yang indah, sejuk, dan asri. Tidak dipungkiri jika Bandung juga menyimpan beberapa surga indah lainnya. 
Destinasi wisata yang potensial penting untuk diketahui oleh masyarakat khususnya turis lokal maupun internasional. Jika destinasi wisata tersebut banyak dikunjungi oleh masyarakat, hal tersebut dapat menambah pendapatan daerah. Selain itu, tempat yang menjadi destinasi wisata akan menjadi lebih terkenal dan dapat berkembang menjadi lebih baik lagi. Untuk mewujudkan hal-hal tersebut dibutuhkan sebuah biro perjalanan wisata yang mengenalkan pengunjung kepada destinasi wisata tersebut.  
Oleh sebab itu, penulis berinisiatif untuk mengembangkan bisnis dalam hal perjalanan wisata. Destinasi wisata yang masih jarang dikunjungi akan menjadi tujuan utama dalam menjalankan bisnis ini. Mengunjungi tempat yang baru akan menjadi hal yang menarik bagi pengunjung. Dalam mengembangkan bisnis ini tentunya memiliki tanggung jawab yang besar. Dimulai dari keamanan, kenyamanan, dan kepuasan yang harus diberikan kepada pengunjung. Pengunjung berhak mendapatkan pelayanan yang baik dan layak dari biro perjalanan wisata tersebut. 
Demi mewujudkan hal yang telah diuraikan tersebut, maka penulis akan merintis bisnis dalam perjalanan wisata dengan nama TOURDERA (Tour Destinasi Wisata Daerah). Dengan adanya TOURDERA akan mempermudah dalam pelayanan ke destinasi wisata baru yang dalam hal ini dapat bermanfaat.baik bagi daerah itu sendiri maupun turis lokal dan internasional. Manfaat lain yang akan didapat berupa lebih dikenalnya daerah destinasi wisata tersebut dan menambah pendapatan daerah agar dapat lebih berkembang lagi kedepannya. Penulis berharap masyarakat akan menjadi lebih tertarik untuk mengunjungi destinasi wisata baru yang masih asri dengan menggunakan jasa biro perjalanan wisata yang kami jalankan.

1.2 Deskripsi Produk
	TOURDERA merupakan suatu aplikasi berbasis web yang menyediakan
layanan informasi mengenai wisata desa potensial yang ada di Bandung dan
sekitarnya dengan mengintegrasikan semua unit pelayanan dan akomodasi yang
mendukung perjalanan pariwisata yang mudah, aman dan terintegrasi dengan
baik. TOURDERA sendiri menyediakan fasilitas pelayanan yang lengkap
dengan kemudahan fitur bagi customer untuk bisa melakukan transaksi dengan
mudah dan cepat.